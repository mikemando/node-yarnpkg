export default function rethrowUnlessCode(maybeError: unknown, ...codes: Array<string>): void;
